

// Instantiate Global Variables
var SoundArray = [];
var record = false;
var play = false;
var startTime = 0;

function playSound(button) {
	new Audio(button + '.mp3').play();
}

function soundClick(button) {
	if (record) {
		var elapsed = Date.now() - startTime;
		publishBeat(elapsed,button);
		updateBeats();
	}
	playSound(button);
}

$(document).ready(function(){
	$("#record").click(function(){
		if (record){  	// Stop Recording
			$(this).css("background","none");
			record = false;
			play   = false;
			updateBeats();
			saveToParse();
 
		} else {   		// Start Recording
			$(this).css("background","red");
			record = true;
			play   = true;
			startTime = Date.now();
			playBack();
		}
	});
 
	$("#play").click(function(){
		if (play){ return; } // Do nothing if already playing
		play = true;
		playBack();
 
	});
 
	$("#stop").click(function(){
		if (play || record){
			if (record){  	// Stop Recording
				$("#record").css("background","none");
				record = false;
				updateBeats();
			}
			play = false;
		}
	});
 
});

function playBack(){
	if (!record){
    	startTime = Date.now();
  	}
  	function playItBack(idx){
	    setTimeout(function(){
	        var item = SoundArray[idx];
	        if (!play || idx >= SoundArray.length){
	        	play = false;
		        return;
	        }
	        while(Date.now() - startTime < item.time){
	          playItBack(idx);
	          return;
	        }
	        playSound(item.button);
	        playItBack(idx+1);
	    }, 5);
	}
	playItBack(0);
}

function compare(a,b) {
  if (a.time < b.time)
    return -1;
  if (a.time > b.time)
    return 1;
  return 0;
}
 
function updateBeats(){
	SoundArray.sort(compare);
	var html = "<tr><th>Time</th><th>Button</th></tr>";
	for (var i=0; i<SoundArray.length; i++){
		html+="<tr><td>" + SoundArray[i].time + ":</td><td>" + SoundArray[i].button + "</td></tr>"
	}
	$("#beats").html(html);
}

////////////
// PubNub //
////////////
pubnub = PUBNUB({                          
    publish_key   : 'pub-c-cdc0ba09-d790-443e-92f8-3d5f1f52781f',
    subscribe_key : 'sub-c-a7ec8b66-2752-11e5-af03-02ee2ddab7fe',
});
 
 
//Subscribe to channel 'demo'
function subscribeTo(){
	pubnub.subscribe({
	    channel: "demo",
	    message: function(m){ //when you receive a message
			//set data button and time to the received message
			var data = {button: m.button, time: m.time}
			//push the received data into our SoundArray
			SoundArray.push(data);
			//call updateBeats() to fix array 
			updateBeats();
		
	    },
	    connect: function(m){
	    		//When we first subscribe we are going to load the song from Parse
          		loadFromParse()
            },
	    error: function (error) {
	      // Handle error here
	      console.log(JSON.stringify(error));
	    }
	 });
}
 
function pubInit(){
  subscribeTo();
}
pubInit();
 
//function to publish our data 
function publishBeat(time, button){
	pubnub.publish({
	    //set channel to demo
	    channel: "demo", 
	    //our message is the beat we are playing
	    message: {time:time, button:button},
	    callback : function(m){console.log(m)}
	});
}

/////////////////////
// Parse Functions //
/////////////////////
 
function loadFromParse(){
  SoundArray.length = 0;  
  var SoundObject = Parse.Object.extend("Sounds");
  var query = new Parse.Query(SoundObject);
  query.find({
    success: function(song) {
      if (song.length) {
        for (var i=0; i<song.length; i++){
            SoundArray = song[i].get('SoundAndTime');
        }
      }
      updateBeats();
    }
  });
}
 
function saveToParse() {
 
  var SoundObject = Parse.Object.extend("Sounds");
  var query = new Parse.Query(SoundObject);
    query.first({
    success: function(song) {
      if (song) {
         song.save(null, {
            success: function (songSave) {
                songSave.set("SoundAndTime", SoundArray);
                songSave.save();
            }
        });
      }
      else{
        var soundObject = new SoundObject();
        soundObject.set("SoundAndTime", SoundArray);
        soundObject.save();
      }
     
    }
  });
}
